# Ingredients

This yields 24 2x2” pieces

305g ap flour
145g pastry flour
110g sugar
1 tsp salt
1 tbsp baking powder
1 lb cold butter
½ cup cold cream
¼ cup cold milk
166.66g Blueberry
½ tsp vanilla/zest

Bake at 325ºF convection oven till edges are brown 18min small 2”x2”
Mix flours baking powder sugar then add salt to combine evenly.
Smash cold butter and meal it into the flour mixture.
Use your fingertips to press butter into the flour until it forms a pea like crumble.

You can use a paddle and mix but on low and slow setting.  Speed will cause friction, and friction will cause heat...which we don’t want.

Add cold milk, cream and stir until ingredients are combined.   Add fillings such as blueberries, nuts, chocolate, etc. to enhance the flavour of your scones.

Press into a brick, either on an open table or in a 8x12 pan. Don’t press too hard - it will settle itself while resting.
Set for 2 hours in fridge or maximum overnight.
Portion into 2x2"pcs.  One recipe makes 24 pcs. Use a cutter (like a ring mold) or cut into squares or triangles with a knife.
Brush an egg-wash and turbinado sugar on top. Orrrrrr brush a little cream and heavy sprinkle granulated sugar before baking.

Bake 160°C for 18-23 minutes.
